import Feature
import paho.mqtt.client as mqtt
import json


class DigitalTwinObject:
    def __init__(self, namespace, name, paho_client: mqtt.Client, definition="", attributes={}, messageReceived=False):
        self.namespace = namespace
        self.name = name
        self.features = {}
        self.paho_client = paho_client
        self.attributes = attributes
        self.definition = definition
        self.messageReceived = messageReceived
        self.create_object()

    def create_object(self):
        thing = {}
        thing['thingId'] = f'{self.namespace}:{self.name}'
        thing['policyId'] = f'{self.namespace}:default'
        thing['attributes'] = self.attributes

        self.sendMessage("/", value=thing)

    def sendMessage(self, path, command="modify", value=None):
        if not self.messageReceived:
            topic = f"{self.namespace}/{self.name}/things/twin/commands/{command}"
            if value:
                payload_json = {"topic": topic, "path": path, "value": value}
            else:
                payload_json = {"topic": topic, "path": path, "value": {}}
            message = self.paho_client.publish(topic, json.dumps(payload_json))
            print(f"Published message to topic {topic} with payload {payload_json}, status={message.rc}")

    def addFeature(self, feature: Feature):
        if feature.name not in self.features.keys():
            self.features[feature.name] = feature
            path = f"/features/{feature.name}"
            dictionary = {"properties": {}}
            for property in feature.properties.keys():
                dictionary["properties"][feature.properties[property].name] = feature.properties[property].value
            self.sendMessage(path, value=dictionary)
        else:
            raise Exception(f"Feature {feature} is already in {self.namespace}:{self.name}")

    def removeFeature(self, feature: Feature):
        if feature.name not in self.features.keys():
            raise Exception(f"Feature {feature} is not in {self.namespace}:{self.name}")
        else:
            del self.features[feature.name]
            path = f"/features/{feature.name}"
            self.sendMessage(path, command="delete")