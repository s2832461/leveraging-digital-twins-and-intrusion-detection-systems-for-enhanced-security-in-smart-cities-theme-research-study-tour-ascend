# Leveraging Digital Twins and Intrusion Detection Systems for enhanced security in smart cities - Theme resarch Study Tour Ascend

## Description
This repository contains all files used for the research Leveraging Digital Twins and Intrusion Detection Systems for enhanced security in smart cities - Theme resarch Study Tour Ascend.

## Installation
Create the venv, install requirements.txt.
To start the docker container, first build it and then use the docker compose file to start the container.
On the physical device, run the script main.py.