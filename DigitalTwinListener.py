import json
import os
from pathlib import Path

import paho.mqtt.client as mqtt
from paho.mqtt.subscribeoptions import SubscribeOptions
import time
import atexit
import requests

from DigitalTwinObject import DigitalTwinObject
from Feature import Feature
from Property import Property

import threading


def new_line_watcher(event):
    if not os.path.isfile("/app/snort/log/alert"):
        Path("/app/snort/log/alert").touch()
    with open("/app/snort/log/alert") as alert_file:
        while not event.is_set():
            line = alert_file.readline()
            if line:
                if "Attack" in line:
                    print(line)
            else:
                time.sleep(1)


def disconnect():
    if paho_client:
        print("Disconnecting paho client")
        paho_client.loop_stop()
        paho_client.disconnect()
    stop_event.set()
    thread.join()


def paho_on_connect(client, userdata, flags, reason, properties):
    print(f"Connected {client}")


def paho_on_connect_fail(client, userdata):
    print("fFailed to connect {client} with userdata {userdata}")


def paho_on_message(client, userdata, message):
    print(f"Received `{message.payload.decode()}` from `{message.topic}` topic")
    parsed = json.loads(message.payload.decode())
    topic = parsed["topic"].split("/")
    thing_namespace = topic[0]
    thing_name = topic[1]
    if object.namespace == thing_namespace and object.name == thing_name:
        object.messageReceived = True
        path = parsed["path"][1:].split("/")
        if len(path) == 1 and path[0] == '':
            path[0] = "/"
        if "value" in parsed:
            value = parsed["value"]
        else:
            value = None
        # Now apply the changes for the thing, based on the action (modify, delete, create) and the path
        action = topic[5]
        if action == "created": # Create a new dt from the values
            print("created")
            if path[0] == "/" and len(path) == 1:
                # we don't accept new dt creation here for simplicity
                return None
            elif len(path) == 1 and path[0] == "attributes": # Add attributes to a thing that does not have attributes yet
                # TODO
                return None
            elif len(path) == 2 and path[0] == "attributes": # Add another attribute to a thing that already has attributes
                # TODO
                return None
            elif path[0] == "definition": # Add a definition to a thing that did not have a definition yet
                # TODO
                return None
            elif path[0] == "features" and len(path) == 1: # Add features to a thing that did not have features yet
                # TODO
                return None
            elif path[0] == "features" and len(path) == 2: # Add a feature to a thing that already had features
                # TODO
                return None
            elif path[0] == "features" and path[2] == "definition": # Add a definition to a feature that did not have any definition yet
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties": # Add properties to a feature that did not have any properties yet
                # TODO
                return None
            elif path[0] == "features" and path[2] == "desiredProperties": # Add desiredProperties to a feature that did not have any properties yet
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Add properties to a feature that already had properties
                # TODO
                return None
            elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Add a desiredProperty to a feature that already had desiredproperties
                # TODO
                return None
            else:
                Exception(f"Got unknown combination of action {action} and path {path} from topic {topic}")
        elif action == "deleted": # Delete a dt, feature or property from the values
            print("delete")
            if path[0] == "/": # Delete a DT
                # We don't accept deleting dt's here
                return None
            elif path[0] == "attributes" and len(path) == 1: # Delete the DT attributes
                object.attributes = {}
            elif path[0] == "attributes" and len(path) == 2: # Delete one specific attribute
                object.attributes.pop(path[1])
            elif path[0] == "definition": # Delete the definition of a thing
                # TODO
                return None
            elif path[0] == "features" and len(path) == 1: # Delete all features
                # TODO
                return None
            elif path[0] == "features" and len(path) == 2: # Delete a single feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "definition": # Delete a definition of a single feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties" and len(path) == 3: # Delete all properties of a feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 3: # Delete all desired properties of a feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Delete one single property
                # TODO
                object.features[path[1]].properties[path[3]].value = value
                return None
            elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Delete one desired property of a feature
                # TODO
                return None
        elif action == "modified": # Modify a dt, feature or property from the values
            print("modify")
            if path[0] == "/": # create or modify a dt
                if object is None:
                    return None
                else: # Modify the dt, replace it with the received spec
                    # TODO
                    return None
            elif path[0] == "attributes" and len(path) == 1: # Overwrite the dt attributes with the ones received in values
                # TODO
                return None
            elif path[0] == "attributes" and len(path) == 2: # Change one specific attribute
                # TODO
                return None
            elif path[0] == "definition": # Set the definition
                # TODO
                return None
            elif path[0] == "features" and len(path) == 1: # Set all features
                # TODO
                return None
            elif path[0] == "features" and len(path) == 2: # set a single feature
                object.removeFeature(object.features[path[1]])
                properties = {}
                for property_value in value['properties'].keys():
                    properties[property_value] = Property(property_value, value['properties'][property_value])
                newFeature = Feature(path[1], object, properties)
            elif path[0] == "features" and path[2] == "definition": # Set a definition of a single feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties" and len(path) == 3: # Set all properties of a feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 3: # Set all desired properties of a feature
                # TODO
                return None
            elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Set one single property
                # TODO
                object.features[path[1]].properties[path[3]].value = value
                return None
            elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Set one desired property of a feature
                # TODO
                return None
            else:
                print(f"Failed to modify with topic {topic} and path {path} and value {value}")
                Exception(f"Failed to modify with topic {topic} and path {path} and value {value}")
        else:
            print(f"Got unknown action {action} from topic {topic}")
            Exception(f"Got unknown action {action} from topic {topic}")
        object.messageReceived = False


def start_mqtt(hostname: str):
    try:
        paho_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
        paho_client.on_connect = paho_on_connect
        paho_client.on_connect_fail = paho_on_connect_fail
        paho_client.connect(hostname)
        paho_client.enable_logger()
        paho_client.on_message = paho_on_message
        options = SubscribeOptions(noLocal=True)
        paho_client.subscribe("infrastructure/#", options=options)
        paho_client.loop_start()
        while not paho_client.is_connected():
            time.sleep(0.1)
    except Exception as e:
        print(f"Failed to connect with exception {e}")
        disconnect()
    return paho_client


if __name__ == '__main__':
    with open("config.json") as config_file:
        config = json.load(config_file)
    paho_client = start_mqtt(config["DOCKER_HOSTNAME"])
    time.sleep(5)  # Wait for connection to be established
    atexit.register(disconnect)
    url = f"http://{config['DOCKER_HOSTNAME']}:8080/api/2/things/infrastructure:car"
    headers = {
        'Authorization': 'Basic <AUTHORIZATION_TOKEN>'
    }
    response = requests.request("GET", url, headers=headers)
    json_data = response.json()
    namespace, name = json_data['thingId'].split(':')
    object = DigitalTwinObject(namespace, name, paho_client, attributes=json_data['attributes'], messageReceived=True)
    for featureName in json_data['features'].keys():
        properties = {}
        for property in json_data['features'][featureName]['properties']:
            properties[property] = Property(property, json_data['features'][featureName]['properties'][property])
        newFeature = Feature(featureName, object, properties)
    object.messageReceived = False
    print("Setup object")
    stop_event = threading.Event()
    thread = threading.Thread(target=new_line_watcher, args=(stop_event,))
    thread.start()
    while paho_client.is_connected():
        time.sleep(1)

