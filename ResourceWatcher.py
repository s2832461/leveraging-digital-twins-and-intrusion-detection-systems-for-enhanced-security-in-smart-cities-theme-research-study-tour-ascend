import matplotlib.pyplot as plt
import time
import docker
import argparse
import csv
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--attack_name', type=str, required=True)
    args = parser.parse_args()
    print("Starting resource collection")

    client = docker.from_env()
    container = client.containers.get("tr")

    virtual_usages = []
    cpu_usages = []
    received_bytes = []
    sent_bytes = []
    stats = container.stats(stream=False)
    previous_sent, previous_received = (stats['networks']['eth0']['tx_bytes'] / (1024) + stats['networks']['eth1']['tx_bytes'] / (1024),
                                        stats['networks']['eth0']['rx_bytes'] / (1024) + stats['networks']['eth1']['rx_bytes'] / (1024))#kB

    time.sleep(1)
    start_time = time.time()
    for i in range(100):
        stats = container.stats(stream=False)
        virtual_usages.append(stats['memory_stats']['usage']/ (1024 * 1024))

        cpu_delta = stats['cpu_stats']['cpu_usage']['total_usage'] - stats['precpu_stats']['cpu_usage']['total_usage']
        system_delta = stats['cpu_stats']['system_cpu_usage'] - stats['precpu_stats']['system_cpu_usage']
        cpu_percent = 0.0
        if system_delta > 0.0:
            cpu_percent = (cpu_delta / system_delta) * stats['cpu_stats']['online_cpus'] * 100.0

        cpu_usages.append(cpu_percent)

        current_sent, current_received = (stats['networks']['eth0']['tx_bytes'] / (1024) + stats['networks']['eth1']['tx_bytes'] / (1024),
                                        stats['networks']['eth0']['rx_bytes'] / (1024) + stats['networks']['eth1']['rx_bytes'] / (1024))#kB
        received_bytes.append(current_received - previous_received)
        sent_bytes.append(current_sent - previous_sent)

        previous_sent, previous_received = current_sent, current_received

        waiting_time = start_time + (i * 3)
        time.sleep(max(0, waiting_time - time.time()))

    x = np.arange(0, 300, 3)

    plt.plot(x, virtual_usages)
    plt.xlabel("time (s)")
    plt.ylabel("memory usage (MB)")
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.savefig(f"output/{args.attack_name}_memory.png")

    plt.clf()
    plt.plot(x, cpu_usages)
    plt.xlabel("time (s)")
    plt.ylabel("cpu usage (%)")
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.savefig(f"output/{args.attack_name}_cpu.png")

    plt.clf()
    plt.plot(x, received_bytes, label='Network received')
    plt.xlabel("time (s)")
    plt.ylabel("Network received (KB)")
    plt.legend()
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.savefig(f"output/{args.attack_name}_network_received.png")

    plt.clf()
    plt.plot(x, sent_bytes, label='Network sent')
    plt.xlabel("time (s)")
    plt.ylabel("Network sent (KB)")
    plt.legend()
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.savefig(f"output/{args.attack_name}_network_sent.png")

    with open(f"output/{args.attack_name}_raw_data_memory.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(virtual_usages)

    with open(f"output/{args.attack_name}_raw_data_cpu.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(cpu_usages)

    with open(f"output/{args.attack_name}_raw_data_network_received.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(received_bytes)

    with open(f"output/{args.attack_name}_raw_data_network_sent.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(sent_bytes)