import DigitalTwinObject
import Property


class Feature:
    def __init__(self, name: str, object: DigitalTwinObject, properties: dict[str: Property]):
        self.name = name
        self.object = object
        self.properties = properties
        for property in self.properties.keys():
            properties[property].setFeature(self)
        self.object.addFeature(self)

    def addProperty(self, property: Property):
        if property.name not in self.properties.keys():
            self.properties[property.name] = property
            path = f"/features/{self.name}/properties/{property.name}"
            value = property.value
            self.object.sendMessage(path, value=value)
        else:
            raise Exception(f"Property {property} is already in {self.namespace}:{self.name}:{self.name}")

    def removeProperty(self, property: Property):
        if property.name in self.properties.keys():
            self.properties.remove(property.name)
            path = f"/features/{self.name}/properties/{property.name}"
            value = property.value
            property.removeFeature()
            self.object.sendMessage(path, value=value, command="delete")
        else:
            raise Exception(f"Property {property} is not in {self.namespace}:{self.name}:{self.name}")
