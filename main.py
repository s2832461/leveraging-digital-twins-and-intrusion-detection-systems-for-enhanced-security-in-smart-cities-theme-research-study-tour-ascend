import time
import json
from typing import Union
import paho.mqtt.client as mqtt
from paho.mqtt.subscribeoptions import SubscribeOptions
import atexit

import requests

from DigitalTwinObject import DigitalTwinObject
from Feature import Feature
from Property import Property

propertyType = Union[str, int]


def create_new_dt(value):
    dt = DigitalTwinObject(value["thingId"].split(":")[0], value["thingId"].split(":")[1], paho_client, attributes=value["attributes"], definition=value["definition"])
    objects.append(dt)
    # Now to add the features and their properties
    for value_feature in value["features"]:
        properties = {}
        for property in value["features"][value_feature]["properties"]:
            properties[property] = Property(property, value["features"][value_feature]["properties"][property])
        Feature(value_feature, dt, properties)


def disconnect():
    if paho_client:
        print("Disconnecting paho client")
        paho_client.loop_stop()
        paho_client.disconnect()


def paho_on_connect(client, userdata, flags, reason, properties):
    print(f"Connected {client}")


def paho_on_connect_fail(client, userdata):
    print(f"Failed to connect {client} with userdata {userdata}")


def paho_on_message(client, userdata, message):
    print(f"Received `{message.payload.decode()}` from `{message.topic}` topic")
    parsed = json.loads(message.payload.decode())
    topic = parsed["topic"].split("/")
    thing_namespace = topic[0]
    thing_name = topic[1]
    thing = None
    for dt in objects:
        if dt.namespace == thing_namespace and dt.name == thing_name:
            thing = dt
            thing.messageReceived = True
            break
    path = parsed["path"][1:].split("/")
    if len(path) == 1 and path[0] == '':
        path[0] = "/"
    if "value" in parsed:
        value = parsed["value"]
    else:
        value = None
    # Now apply the changes for the thing, based on the action (modify, delete, create) and the path
    action = topic[5]
    if action == "created": # Create a new dt from the values
        print("created")
        if path[0] == "/" and len(path) == 1:
            create_new_dt(value)
        elif len(path) == 1 and path[0] == "attributes": # Add attributes to a thing that does not have attributes yet
            # TODO
            return None
        elif len(path) == 2 and path[0] == "attributes": # Add another attribute to a thing that already has attributes
            # TODO
            return None
        elif path[0] == "definition": # Add a definition to a thing that did not have a definition yet
            # TODO
            return None
        elif path[0] == "features" and len(path) == 1: # Add features to a thing that did not have features yet
            # TODO
            return None
        elif path[0] == "features" and len(path) == 2: # Add a feature to a thing that already had features
            # TODO
            return None
        elif path[0] == "features" and path[2] == "definition": # Add a definition to a feature that did not have any definition yet
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties": # Add properties to a feature that did not have any properties yet
            # TODO
            return None
        elif path[0] == "features" and path[2] == "desiredProperties": # Add desiredProperties to a feature that did not have any properties yet
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Add properties to a feature that already had properties
            # TODO
            return None
        elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Add a desiredProperty to a feature that already had desiredproperties
            # TODO
            return None
        else:
            Exception(f"Got unknown combination of action {action} and path {path} from topic {topic}")
    elif action == "deleted": # Delete a dt, feature or property from the values
        print("delete")
        if path[0] == "/": # Delete a DT
            objects.remove(thing)
        elif path[0] == "attributes" and len(path) == 1: # Delete the DT attributes
            thing.attributes = {}
        elif path[0] == "attributes" and len(path) == 2: # Delete one specific attribute
            thing.attributes.pop(path[1])
        elif path[0] == "definition": # Delete the definition of a thing
            # TODO
            return None
        elif path[0] == "features" and len(path) == 1: # Delete all features
            # TODO
            return None
        elif path[0] == "features" and len(path) == 2: # Delete a single feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "definition": # Delete a definition of a single feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties" and len(path) == 3: # Delete all properties of a feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 3: # Delete all desired properties of a feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Delete one single property
            # TODO
            thing.features[path[1]].properties[path[3]].value = value
            return None
        elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Delete one desired property of a feature
            # TODO
            return None
    elif action == "modified": # Modify a dt, feature or property from the values
        print("modify")
        if path[0] == "/": # create or modify a dt
            if thing is None:
                create_new_dt(value)
            else: # Modify the dt, replace it with the received spec
                # TODO
                return None
        elif path[0] == "attributes" and len(path) == 1: # Overwrite the dt attributes with the ones received in values
            # TODO
            return None
        elif path[0] == "attributes" and len(path) == 2: # Change one specific attribute
            # TODO
            return None
        elif path[0] == "definition": # Set the definition
            # TODO
            return None
        elif path[0] == "features" and len(path) == 1: # Set all features
            # TODO
            return None
        elif path[0] == "features" and len(path) == 2: # set a single feature
            thing.removeFeature(thing.features[path[1]])
            properties = {}
            for property_value in value['properties'].keys():
                properties[property_value] = Property(property_value, value['properties'][property_value])
            newFeature = Feature(path[1], thing, properties)
        elif path[0] == "features" and path[2] == "definition": # Set a definition of a single feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties" and len(path) == 3: # Set all properties of a feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 3: # Set all desired properties of a feature
            # TODO
            return None
        elif path[0] == "features" and path[2] == "properties" and len(path) == 4: # Set one single property
            # TODO
            thing.features[path[1]].properties[path[3]].value = value
            return None
        elif path[0] == "features" and path[2] == "desiredProperties" and len(path) == 4: # Set one desired property of a feature
            # TODO
            return None
        else:
            print(f"Failed to modify with topic {topic} and path {path} and value {value}")
            Exception(f"Failed to modify with topic {topic} and path {path} and value {value}")
    else:
        print(f"Got unknown action {action} from topic {topic}")
        Exception(f"Got unknown action {action} from topic {topic}")
    thing.messageReceived = False


def start_mqtt(hostname: str):
    try:
        paho_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
        paho_client.on_connect = paho_on_connect
        paho_client.on_connect_fail = paho_on_connect_fail
        paho_client.connect(hostname)
        paho_client.enable_logger()
        paho_client.on_message = paho_on_message
        options = SubscribeOptions(noLocal=True)
        paho_client.subscribe("infrastructure/#", options=options)
        paho_client.loop_start()
        while not paho_client.is_connected():
            time.sleep(0.1)
    except Exception as e:
        print(f"Failed to connect with exception {e}")
        disconnect()
    return paho_client


def create_policy(hostname: str):
    policy = {}
    policy["policyId"] = "infrastructure:default"
    policy["entries"] = {}
    policy["entries"]["owner"] = {}
    policy["entries"]["owner"]["subjects"] = {}
    policy["entries"]["owner"]["subjects"]["nginx:ditto"] = {}
    policy["entries"]["owner"]["subjects"]["nginx:ditto"]["type"] = "nginx basic auth user"
    policy["entries"]["owner"]["resources"] = {}
    policy["entries"]["owner"]["resources"]["thing:/"] = {}
    policy["entries"]["owner"]["resources"]["thing:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["owner"]["resources"]["thing:/"]["revoke"] = []
    policy["entries"]["owner"]["resources"]["policy:/"] = {}
    policy["entries"]["owner"]["resources"]["policy:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["owner"]["resources"]["policy:/"]["revoke"] = []
    policy["entries"]["owner"]["resources"]["message:/"] = {}
    policy["entries"]["owner"]["resources"]["message:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["owner"]["resources"]["message:/"]["revoke"] = []

    policy["entries"]["connection"] = {}
    policy["entries"]["connection"]["subjects"] = {}
    policy["entries"]["connection"]["subjects"]["connection:mosquitto-mqtt"] = {}
    policy["entries"]["connection"]["subjects"]["connection:mosquitto-mqtt"][
        "type"] = "Connection to mosquitto MQTT broker"
    policy["entries"]["connection"]["resources"] = {}
    policy["entries"]["connection"]["resources"]["thing:/"] = {}
    policy["entries"]["connection"]["resources"]["thing:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["connection"]["resources"]["thing:/"]["revoke"] = []
    policy["entries"]["connection"]["resources"]["policy:/"] = {}
    policy["entries"]["connection"]["resources"]["policy:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["connection"]["resources"]["policy:/"]["revoke"] = []
    policy["entries"]["connection"]["resources"]["message:/"] = {}
    policy["entries"]["connection"]["resources"]["message:/"]["grant"] = ["READ", "WRITE"]
    policy["entries"]["connection"]["resources"]["message:/"]["revoke"] = []

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic <AUTHORIZATION>'
    }

    response = requests.request("PUT", f"http://{hostname}:8080/api/2/policies/infrastructure:default",
                                headers=headers, data=json.dumps(policy))
    print(response.text)


def create_connection(hostname: str):
    payload = json.dumps({
        "name": "mosquitto",
        "connectionType": "mqtt-5",
        "connectionStatus": "open",
        "failoverEnabled": True,
        "uri": "tcp://mosquitto:1883",
        "sources": [
            {
                "addresses": [
                    "infrastructure/#"
                ],
                "authorizationContext": [
                    # "connection:mosquitto-mqtt"
                    "nginx:ditto"
                ],
                "qos": 1
            }
        ],
        "targets": [
            {
                "address": "infrastructure/{{ thing:id }}",
                "topics": [
                        "_/_/things/twin/events",
                        "_/_/things/live/messages",
                        "_/_/things/live/commands"
                ],
                "authorizationContext": [
                    # "connection:mosquitto-mqttt"
                    "nginx:ditto"
                ],
                "qos": 1
            }
        ]
    })
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic <AUTHORIZATION>'
    }

    response = requests.request("PUT", f"http://{hostname}:8080/api/2/connections/mosquitto", headers=headers, data=payload)

    print(response.text)


if __name__ == "__main__":
    with open("config.json") as config_file:
        config = json.load(config_file)
    objects = []
    hostname = config["HOSTNAME"]
    create_policy(hostname)
    create_connection(hostname)
    paho_client = start_mqtt(hostname)
    time.sleep(5)  # Wait for connection to be established
    atexit.register(disconnect)
    dt = DigitalTwinObject("infrastructure", "car", paho_client, attributes={'manufacturer': 'Volvo'})
    objects.append(dt)
    property = Property("speed", "100")
    feature = Feature("car", dt, {property.name: property})
    time.sleep(2)
    while paho_client.is_connected():
        time.sleep(1)
