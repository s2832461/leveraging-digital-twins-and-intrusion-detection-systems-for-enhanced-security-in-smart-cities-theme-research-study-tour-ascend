FROM ubuntu:latest
LABEL authors="Erwin Loof"

WORKDIR /app
COPY . .
ENV PYTHONUNBUFFERED=1
RUN apt-get update
RUN apt-get install snort -y
RUN apt-get install python3-full -y
RUN apt-get install python3-pip -y
RUN /usr/bin/python3 -m venv /app/venv
RUN /app/venv/bin/pip install -r requirements.txt
RUN mkdir /app/output
CMD ["/app/start.sh"]