#!/bin/zsh

echo "starting attack icmp"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=icmp &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S hping3 --icmp --flood <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_icmp

echo "starting attack syn"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=syn &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S hping3 -S -p 1883 --flood <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_syn

echo "starting attack rst"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=rst &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S hping3 -R -p 1883 --flood <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_rst

echo "starting attack ack"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=ack &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S hping3 -A -p 1883 --flood <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_ack

echo "starting attack udp"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=udp &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S hping3 --udp -p 1883 --flood <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_udp

echo "starting attack nmap"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=nmap &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S nmap <IP_address_of_digital_twin_docker_container>"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_nmap

echo "starting attack nmap_tcp"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=nmap_tcp &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S nmap <IP_address_of_digital_twin_docker_container> -sS"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_nmap_tcp

echo "starting attack nmap_aggressive"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=nmap_aggressive &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S nmap <IP_address_of_digital_twin_docker_container> -A"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_nmap_aggressive

echo "starting attack nmap_udp"
sudo docker exec tr sh -c "> /var/log/snort/alert" &
sudo docker exec tr /app/venv/bin/python3 ResourceWatcher.py --attack_name=nmap_udp &
sudo -u <local_username> ssh kali@<kali_virtual_machine_ip_address> "echo kali | timeout 305 sudo -S nmap <IP_address_of_digital_twin_docker_container> -sU"

sudo docker exec tr cp /var/log/snort/alert /app/output/snort_alerts_nmap_udp
