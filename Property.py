import Feature


class Property():
    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value
        self.feature = None

    def setValue(self, newValue: str):
        self.value = newValue
        self.feature.object.sendMessage(f"/features/{self.feature.name}/properties/{self.name}", value=self.value)

    def setFeature(self, feature: Feature):
        self.feature = feature

    def removeFeature(self):
        self.feature = None